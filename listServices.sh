#!/bin/bash

set -x
DIR_STORAGE=/datastore
FILE=serviceList.txt
DIR_FILE=$DIR_STORAGE/$FILE

rm -rf $DIR_STORAGE/*
cd $DIR_STORAGE
touch $FILE

#CHECK ALL SERVICES IN CHKCONFIG AND GETS THE COLUMN
for i in `chkconfig --list | awk '{print $1}'`

#LOOP TO CHECK THE STATUS OF EACH SERVICE THAT ARE COLLECTED FROM THE FOR STATEMENT
#IT THEN CHECKS EACH OF THEIR VERSIONS USING THE RPM
do
        status=`/sbin/service $i status`
        packagename=`rpm -qf /etc/init.d/$i`

#CHECKS ONLY FOR SERVICES WITH RUNNING STATUS
if echo "$status" | grep -q running

then

#SAVES THE LIST OF THE SERVICES WITH THEIR VERSIONS
echo $i"-->"$packagename >> $DIR_FILE

fi
done