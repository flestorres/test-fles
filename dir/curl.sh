#!/bin/bash
set -e

SECONDS=0
domain="https://dev-ecc.toroserver.com"
echo "[INFO] Checking if $domain is reachable ..."
while [[ $SECONDS -lt 301 ]]; do
  curlCommand=$(curl -L -s -o /dev/null -w ''%{http_code}'' $domain)
  if [[ $curlCommand != 200 ]] ; then
  sleep 1s

  elif [[ $curlCommand == 200 ]] ; then
  echo "[INFO] Domain is reachable."
  echo "[INFO] Component status will be updated."
  sh /datastore/library/devops-scripts/lib/status.sh dev-ecc operational
  exit 0
  fi
done

echo "[INFO] Checking one last time..."
if [[ $curlCommand != 200 ]] ; then
  echo "[ERROR] Domain is unreachable."
  echo "[ERROR] Component status will be updated."
  echo "[ERROR] Incident will be posted."
  sh /datastore/library/devops-scripts/lib/status.sh dev-ecc partialoutage "Domain unreachable!" investigating "Please check on instance logs to troubleshoot." dev-ecc
  exit 1
fi