#!/bin/bash
#Description: Determine a branch if it is a mainline or sub, and extract its version from the pom.xml
set -e
file='build.gradle'

determineBranch () {

    if [[ "${bamboo.planRepository.branchName}" = "hotfix-$version" ]] || [[ "${bamboo.planRepository.branchName}" = "release-$version" ]] || [[ "${bamboo.planRepository.branchName}" = "develop" ]] || [[ "${bamboo.planRepository.branchName}" = "master" ]]; then
        echo "[INFO] Branch name is ${bamboo.planRepository.branchName}."
        echo "[INFO] Detected version is at $version! Proceeding..."
        echo "[INFO] It is a mainline branch"
        gradle clean test --refresh-dependencies
        gradle install
        gradle upload
    fi

    if [[ "${bamboo.planRepository.branchName}" != "hotfix-$version" ]] && [[ "${bamboo.planRepository.branchName}" != "release-$version" ]] && [[ "${bamboo.planRepository.branchName}" != "develop" ]] && [[ "${bamboo.planRepository.branchName}" != "master" ]]; then
        echo "[INFO] Branch name is ${bamboo.planRepository.branchName}."
        echo "[INFO] Detected version is at $version! Proceeding..."
        echo "[INFO] It is a sub branch."
        gradle clean test
    fi
}

if grep -q '<revision>' $file; then
    echo "[INFO] Found <revision> tag, checking version now..."
    version=$(cat $file | grep "<revision>" | cut -f2 -d">" | cut -f1 -d"<" | cut -f1 -d"-" | head -n 1)

        determineBranch

elif grep -q '<version>' $file && ! grep -q '<revision>' $file; then
    echo "[INFO] Found <version> tag, checking version now..."
    version=$(cat $file | grep "<version>" | cut -f2 -d">" | cut -f1 -d"<" | cut -f1 -d"-" | head -n 1)

        determineBranch

else
    echo "[ERROR] Cannot find version!"
    exit 1

fi