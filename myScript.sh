#!/bin/bash
set -e

determineBranch () {

    if [[ "${bamboo.planRepository.branchName}" = "hotfix-$version" ]] || [[ "${bamboo.planRepository.branchName}" = "release-$version" ]] || [[ "${bamboo.planRepository.branchName}" = "develop" ]] || [[ "${bamboo.planRepository.branchName}" = "master" ]]; then
        echo "[INFO] Branch name is ${bamboo.planRepository.branchName}."
        echo "[INFO] It is a mainline branch"
    fi

    if [[ "${bamboo.planRepository.branchName}" != "hotfix-$version" ]] && [[ "${bamboo.planRepository.branchName}" != "release-$version" ]] && [[ "${bamboo.planRepository.branchName}" != "develop" ]] && [[ "${bamboo.planRepository.branchName}" != "master" ]]; then
        echo "[INFO] Branch name is ${bamboo.planRepository.branchName}."
        echo "[INFO] It is a sub branch."
    fi
}

if grep -q '<revision>' pom.xml; then
    version=$(cat pom.xml | grep "<revision>" | cut -f1 -d"-" | cut -f2 -d">")

        determineBranch

elif grep -q '<version>' pom.xml && ! grep -q '<revision>' pom.xml; then
    version=$(cat pom.xml | grep "<version>" | cut -f2 -d">" | cut -f1 -d"<" | head -n 1)

        determineBranch

else
    echo "[ERROR] Cannot find version!"
    exit 1

fi